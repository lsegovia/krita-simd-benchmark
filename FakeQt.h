/*
 *  SPDX-FileCopyrightText: 2021 Dmitry Kazakov <dimula73@gmail.com>
 *
 *  SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef FAKEQT_H
#define FAKEQT_H

#include <cstdint>

using quint8 = uint8_t;
using quint16 = uint16_t;
using quint32 = uint32_t;
using quint64 = uint64_t;

using qint8 = int8_t;
using qint16 = int16_t;
using qint32 = int32_t;
using qint64 = int64_t;

using qreal = double;

#define Q_UNUSED(x) (void)x;

#endif // FAKEQT_H
